require("@babel/polyfill");
const ArgumentType = require('../../extension-support/argument-type');
const BlockType = require('../../extension-support/block-type');
const tf = require('@tensorflow/tfjs');
const BrainText = require('brain-text');
const Runtime = require('../../engine/runtime');
const Cast = require('../../util/cast');
const v4 = require('uuid');
const log = require('../../util/log');
const cast = require('../../util/cast');
const formatMessage = require('format-message');
const Base64Util = require('../../util/base64-util');

/**
 * Icon png to be displayed at the left edge of each extension block, encoded as a data URI.
 * @type {string}
 */
// eslint-disable-next-line max-len
const blockIconURI = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAACXBIWXMAABYlAAAWJQFJUiTwAAAKcElEQVR42u2cfXAU9RnHv7u3L3d7l9yR5PIGXO7MkQKaYiCUWqJhFGvRMk4JZXSc8aXVaSmiYlthVHQEW99FxiIdrVY6teiMdoa+ICqhIqgQAsjwMgYDOQKXl7uY17u9293b3f5x5JKYe8+FJGSfvzbP/n77e/azz+95nt9v90KoqgpN0hdSQ6AB1ABqADWAmmgANYAaQA2gJhpADeBEE2q8GPLaWzu/CslyiY4k9dOn5uijtXGd7+jWkaReVpT3Hrhv6d0awEFC07rgD+ZeYYnXprhwigUAvjj0zbjxQCLebozT7iDzK1ZUWCru2K7L//6MVC8ue45Blz8n6rlQ815QtuohOlXiEdy/AUqPa6y59Mkh6Q1345GNja6m7pHEQKNl3t0704EXat4L6fSOmOeEI1vHKzwAyNJR9MPFpRUPOu0ONm2A0xatWaTLm5WfDrzvAppA8AbiG03fC8CQNkDKZK2YrPAuRrhpifJERsuYywveJc7CqcIDMAyeLm82dEXzw39I/qjXkpr3QuW9lxfAdOABGAKPslWDnbsy7Jl8BxTeM3SqmO0gaA5U6c3jymup0YSn9JyLee67wpTfBQAQjmyF3HFqiJcRtDECjy5dAmbmcgQPvjjxl3Lx4IVjnD/5cE1zkWtyP34VBGcdKLJnLgc9cznk1kMXFdzEn8KJ4KUqqsSHvcxWDf7j1UM8UPr6/YgHhhX8xAaYaXgAIB7fBnbuSrBzV8aNgarEQ/z6/YkLcDTg9V9XlXjQtuqoU1TpcUHlvZDOfDiuyh5qPMCLrJ1bDw3EuUtx81N/BH3pjQBJQ2HMF5V6iKfeRchVm9kkMtrwxmSdobeA9daBde8GwVlBcFYofS1Jw0vaAy9HeJHQwBUPzIBvGxDc92Rmp/BowJs10wkAONfsBs8HAAAltqngOAO8HZ3o6OiMqcvLy4E1Lwc8H8C5ZndMXdLJa/qNacNLCDBw/O8nFUNWxp/64+tWAwBefe1tHKg7CgC4/9d3ori4EHv3HcDrb26PqVt2602ovvaHaGlpw+8ffSamLqXYmya8jG8mpFy6iGLkWLh4HAwG4+r6j4VBfaPpLgU8IMGO9MLqW2pYQ9aQokuR5dgXIwCC1CUcNMj3hpdvLAdSF54EYpCHooRA0Swomo2pC0kCQpIAkqTA6LmYupgxL0X7m78+aG10NXVkpIwxsAwWXncDCESHLkohfPbpbiT6ZFPPZQ9fC0e58Wi6wTDj6UbT/rQAyiERS2pW4Kc3LQDLRO8miCEAKj7d83FcTxyLJJJJ+9MCqKoq9HomMrgkSThxsgEcZ8AMpwMkSYJlKDA0DVUFiHGWRDJp/4jXwqIo4uFHnkZXdw8AYGbZFXhs3WqQJDkhkkim7E8KoMlkxKbnn8DBunrwUli3e8/+yOAA0HjmHDq7upGXm5PUoDUr7hmWRB5Zt3FYwoime+vtd/H6G9uGJIxouniSyP6H7v8FystnY80jGzIA0MihsMAKu20aTp3JzFb6WCWRuDUvHwByw8cOhw2FBVaYjNzIAba1e3Hfb9aiq7MTNStuBwAsvr4KO3d9GnmKztIS5EyxTJiVSDT7p04tipx/9MnnYc7ORlu7NzMxsK3di5AkDHgGw2DTC+uHBeGJshJJZL/fxyMQEDKbRAiCQDAoQhBDYBkKNE2j4uqrhpUBoiSBIMZfEhkN+1NeiWSqEB2rlUg69md0JRIQRHy86z8jXsqNVRLJlP0jqgNJXXgAgjbCcONmCHUvQ+44NWG2s/rtH5Mt/ciToo0wLH4JBGO6LLazRiJk2vBYy4gHHw/bWSN+LZBKEhkMjzn/CaSiKgQOvJDyFB7L7axUJWNJZDA8IhQA1boPin7KZbMSGfUYyFx9b3hXg/cCsoBA2Z0AoYOaxlcC4+mdyCUDKBzanLFBJ3USyaRMuiSSKZmUSSSTMimTCABUlblRU9kAZ0E39p+eii21c+EL0jHbOwu6sfaWgyjND//U4oP6MmzZnfi79XT7mfQSNi7bh0JzOLG19XBY/89r49pYVebGqhuOosDsh1+gsWV3BXYdd2Q+BlaVuXFv9bHgkSbzk+vfcVRyjHhi47J9cftsXLYf7T36Ix8cLHlo6ydlv6qpPI2qssRZcuOy/Wjp4k5s+2zG+offKqtcUt6kJtNv7S0H0RtkvEufXTB/6bML5je2Wy7UVDbEbF9o9mPDsv2oP5v75vbPS26rP5u3fdXiozDppcwDrKlswOlWy9E//DX09Mt/azh8zzNM1RybF86C7pheVGD240CDeX3NWtfml94Rt+0+Mf3Lm8qbEnpfgdmPs+3G9+564vTT//pM/GrHYduWRP0AYOEMN/5S61xT92Vtfd2XtfWb/vu91fHALyxzw9tnkB/cTD5w+2Ou9375HHtfa7exM5mxRpKFaafdQQKgAcDERs98/foLHrXdaXfoABi8vczhWO2/28/TRR5z2h00gKymNl1ton79oigq6bQ7dE67Q+ew9mb1h4FYYwVESgLAXLSRa+3mWpIdK+UYuPiq89f8+XfT/+ftZQ4vLm9ZmUyfdcsv1M2fWfRaUCK8i8vdK1u6ktuAWPWTsztm24o/cnnYHUsrWzd1+fVJ9XtqxbG3XzFdNcPTawjcueibpxK1t+X26f/9R8a953jub4typOvm2b1XnvUmv8JKWMZcaZffX3XDERRP8cGaFRjWxtPLoZvXY4oxgPBNEsgxBhCUKEzL6Ru+JydS8Ak0giKFgESDJFQoKmCgQzAwIfQEWETzmoBIwd2VNaStu8uEHGO4Buz06zHHFv0dRkefAZ1+PQx0KNK2eIoPLCUj2zDc275qzgcBFWv+cf3IyxgTK2KOzQufEM5kfpGF12eGPSf8DXN+No/87HDWiwYYALw+M6ym8AscAxO++X7xCTRM7EDQzht0Da8v/NWo1dQDAxNCocUXs+303IGHdaptOmYXnh/SLlZbV+fwnwJm6UXEm/ojqgM/PFmJQ81OPHfrtqT7bN23BE8seTflYLvz5DwYGQHLKz5Puo/XZ8aLtT+D1dSDuxbsGQIymmz48DbwIguOESJOcce8XaO3oVpZ8k3Em5KVVAAMFnuOB9as1MbimCBunn04vBmR40ls29Wfgxf1KMn1gBdY+MXUCvK4ANvPndpLzrLzALjBN2VPwrDBksgLYkn1jBMp90nVY2++8vAw3RlPeLNYVZSPAEgjKWP6ZCn4lF+gMdnE08spQb73RQB9aXtgo6tJcNodf8rWz3L//Br340UW3sExEkXrFFKSSUVHqkRfkJZ8QSZk5gS6hw9H+GyDQAclSs41BVmSUIn+toAKIUTJskKoQUknCxKlkISKb/sM0NMyyVAhXW+AlYosfgOgQlUJVadTSUWBKoQoudvPioPbenq5oIUTaRUqenhWKi3oyVIUqKpKREoLggDhF6hQb4CV9LRM9rctMPN6glChp2SdTqeSskwoAECSKnG61fzFR/XsGu+FhmONriYl7TImsjoYKJyZSeB8CoBQo6spqU8TCO1fgE7gDVUNoCYaQA2gBlADqAHURAOoAdQAagA10QCOgfwfNp/hXbfBMCAAAAAASUVORK5CYII=';

class Scratch3LearningMLBlocksTexts {
    /**
     * @return {string} - the name of this extension.
     */
    static get EXTENSION_NAME() {
        return 'learningml-texts';
    }

    /**
     * @return {string} - the ID of this extension.
     */
    static get EXTENSION_ID() {
        return 'learningmlTexts';
    }

    /**
     * Construct a set of Echidna blocks.
     * @param {Runtime} runtime - the Scratch 3.0 runtime.
     */
    constructor(runtime) {
        this.modelFunction = null;
        this.runtime = runtime;
        this.brainText = new BrainText();
        this.runtimeState = "STOPPED";
        this.getModelFromEasyML();


        // this is needed in order to know when the project is running since
        // I want to retrain only when project is running (see retrain() function)
        this.runtime.on(Runtime.PROJECT_RUN_START, () => {
            console.log("Entro en run start");
            this.runtimeState = "RUNNING";
        });

        this.runtime.on(Runtime.PROJECT_RUN_STOP, () => {
            console.log("Entro en run stop");
            this.runtimeState = "STOPPED";
        });
    }

    buildModel(model) {
        console.log("entro en buildModel");
        let net = model.modelJSON.net;
        // Atention TRICK: When serialized, if timeout=Infinity, as JSON don't 
        // understand Infinity value is saved as 0 which causes an error when 
        // building net fromJSON. So, this is fixed here (I don't like this solution)
        if (net.trainOpts.timeout != undefined) {
            net.trainOpts.timeout =
                (net.trainOpts.timeout == 0) ? Infinity : net.trainOpts.timeout;
        }

        this.brainText.fromJSON(model.modelJSON);

        this.modelFunction = function(entry) {
            let result = this.brainText.run(entry);
            console.log(result);
            return result
        }

    }

    getInfo() {
        return {
            id: Scratch3LearningMLBlocksTexts.EXTENSION_ID,
            name: Scratch3LearningMLBlocksTexts.EXTENSION_NAME,
            //blockIconURI: blockIconURI,
            showStatusButton: false,
            blocks: [

                {
                    opcode: 'classifyText',
                    blockType: BlockType.REPORTER,
                    text: formatMessage({
                        id: 'learningml.classifytext',
                        default: 'Classify text [TEXT]',
                        description: 'classify a text'
                    }),
                    arguments: {
                        TEXT: {
                            type: ArgumentType.STRING,
                            defaultValue: 'text'
                        }
                    }
                },
                {
                    opcode: 'confidenceText',
                    blockType: BlockType.REPORTER,
                    text: formatMessage({
                        id: 'learningml.confidencetext',
                        default: 'Confidence for text [TEXT]',
                        description: 'Confidence of a classificatiom'
                    }),
                    arguments: {
                        TEXT: {
                            type: ArgumentType.STRING,
                            defaultValue: 'text'
                        }
                    }
                },
                {
                    opcode: 'statusText',
                    blockType: BlockType.REPORTER,
                    text: formatMessage({
                        id: 'learningml.modelstatus',
                        default: 'Model status',
                        description: 'Model status'
                    }),
                },
                {
                    opcode: 'addTextToLabel',
                    blockType: BlockType.COMMAND,
                    text: formatMessage({
                        id: 'learningml.addtexttolabel',
                        default: 'Add text [ENTRY] to label [LABEL]',
                        description: 'Add a text to a label'
                    }),
                    arguments: {
                        ENTRY: {
                            type: ArgumentType.STRING,
                            defaultValue: "text"
                        },
                        LABEL: {
                            type: ArgumentType.STRING,
                            defaultValue: "label"
                        }
                    }
                },
                {
                    opcode: 'trainAndWait',
                    blockType: BlockType.COMMAND,
                    text: formatMessage({
                        id: 'learningml.learnandwait',
                        default: 'Learn from texts dataset and wait',
                        description: 'Learn from texts dataset and wait'
                    }),
                }

            ]
        };
    }


    myReporter(args) {
        return args.TEXT.charAt(args.LETTER_NUM);
    };

    loadTensorflowModel(args) {
        tf.tensor([1, 2, 3, 4]).print();
        const model = tf.loadLayersModel('indexeddb://image-model')
            .then(m => {
                console.log(m);
                console.log(model);
            });
        console.log(model);
        console.log(args);
    }

    sincroLocalModel(model) {
        console.log(this.runtime.easyml_model);
        console.log(model)

        // this conditional chain can be simplified. However, I prefer keeping it 
        // like this because it is easier to be interpreted
        if (model && this.runtime.easyml_model) {
            // this.runtime.easyml_model has to be updated with model
            // if they are diferent
            if (model.id != this.runtime.easyml_model.id) {
                this.runtime.easyml_model = model;
                this.modelFunction = null; // force to update the modelFunction
            }
        } else if (model && !this.runtime.easyml_model) {
            // this.runtime.easyml_model has to be updated
            this.runtime.easyml_model = model;
            this.modelFunction = null; // force to update the modelFunction
        } else if (!model && this.runtime.easyml_model) {
            // nothing to do
            console.log("nothing to do");

        } else { // !model && !this.runtime.easyml_model
            // nothing to do
            console.log("nothing to do");
        }
    }


    sincroModelFunction() {
        if (this.modelFunction == null) {
            console.log("building modelFunction with this model");
            console.log(this.runtime.easyml_model);
            this.buildModel(this.runtime.easyml_model);
        }
    }

    getModelFromEasyML() {
        return new Promise((resolve, reject) => {
            let model = localStorage.getItem("easyml_model");

            if (model == null) {
                reject("no model");
                return;
            }
            let modelObj = JSON.parse(model);
            console.log(modelObj);
            this.sincroLocalModel(modelObj);
            if (!this.runtime.easyml_model) {
                reject("You must train a model before using me");
            } else {
                this.sincroModelFunction();
                resolve(modelObj);
            }

        });
    }

    classifyText(args) {

        console.log("AQUI");
        return this.getModelFromEasyML().then(
            r => {
                const text = Cast.toString(args.TEXT);
                let result = this.modelFunction(text)['label']
                return result;
            },
            e => {
                return e;
            }
        );
    }

    confidenceText(args) {

        return this.getModelFromEasyML().then(
            r => {
                const text = Cast.toString(args.TEXT);
                let result = this.modelFunction(text)['confidence']
                return result;
            },
            e => {
                return e;
            }
        );
    }

    addTextToLabel(args) {
        if (!localStorage.getItem("easyml_model")) return;

        let entry = args.ENTRY;
        let label = args.LABEL;

        this.brainText.addOneData({ label: label, text: entry })
        let ch = new BroadcastChannel('model-channel');
        ch.postMessage({ label: label, data: entry });
        this.updateModel("UNTRAINED")
    }

    async trainAndWait(args) {
        if (!localStorage.getItem("easyml_model")) return;
        let m = await this.brainText.train();
        this.updateModel("TRAINED");
    }

    statusText() {
        return this.getModelFromEasyML().then(
            model => {
                console.log(parent);
                return model.state;
            }
        );
    }

    updateModel(state) {
        this.id = v4();
        console.log('Updating model storage');
        let model = this.toJSON(state);
        localStorage.setItem("easyml_model", model);

    }

    toJSON(state) {
        let m = {
            id: this.id,
            state: state,
            modelJSON: this.brainText.toJSON()
        }
        return JSON.stringify(m);
    }

}

module.exports = Scratch3LearningMLBlocksTexts;