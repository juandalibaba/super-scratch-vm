const ArgumentType = require('../../extension-support/argument-type');
const BlockType = require('../../extension-support/block-type');
const tf = require('@tensorflow/tfjs');
const mobilenetModule = require('@tensorflow-models/mobilenet');
const base64js = require('base64-js');
const log = require('../../util/log');
const cast = require('../../util/cast');
const formatMessage = require('format-message');

const IMAGE_SIZE = 227;

const VideoState = {
    /** Video turned off. */
    OFF: 'off',

    /** Video turned on with default y axis mirroring. */
    ON: 'on',

    /** Video turned on without default y axis mirroring. */
    ON_FLIPPED: 'on-flipped'
};

const InputType = {
    VIDEO: 'video',
    COSTUME: 'costume',
    TEXT: 'text',
    NUMBERS: 'numbers'
}


/**
 * Icon png to be displayed at the left edge of each extension block, encoded as a data URI.
 * @type {string}
 */
// eslint-disable-next-line max-len
const blockIconURI = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAACXBIWXMAABYlAAAWJQFJUiTwAAAKcElEQVR42u2cfXAU9RnHv7u3L3d7l9yR5PIGXO7MkQKaYiCUWqJhFGvRMk4JZXSc8aXVaSmiYlthVHQEW99FxiIdrVY6teiMdoa+ICqhIqgQAsjwMgYDOQKXl7uY17u9293b3f5x5JKYe8+FJGSfvzbP/n77e/azz+95nt9v90KoqgpN0hdSQ6AB1ABqADWAmmgANYAaQA2gJhpADeBEE2q8GPLaWzu/CslyiY4k9dOn5uijtXGd7+jWkaReVpT3Hrhv6d0awEFC07rgD+ZeYYnXprhwigUAvjj0zbjxQCLebozT7iDzK1ZUWCru2K7L//6MVC8ue45Blz8n6rlQ815QtuohOlXiEdy/AUqPa6y59Mkh6Q1345GNja6m7pHEQKNl3t0704EXat4L6fSOmOeEI1vHKzwAyNJR9MPFpRUPOu0ONm2A0xatWaTLm5WfDrzvAppA8AbiG03fC8CQNkDKZK2YrPAuRrhpifJERsuYywveJc7CqcIDMAyeLm82dEXzw39I/qjXkpr3QuW9lxfAdOABGAKPslWDnbsy7Jl8BxTeM3SqmO0gaA5U6c3jymup0YSn9JyLee67wpTfBQAQjmyF3HFqiJcRtDECjy5dAmbmcgQPvjjxl3Lx4IVjnD/5cE1zkWtyP34VBGcdKLJnLgc9cznk1kMXFdzEn8KJ4KUqqsSHvcxWDf7j1UM8UPr6/YgHhhX8xAaYaXgAIB7fBnbuSrBzV8aNgarEQ/z6/YkLcDTg9V9XlXjQtuqoU1TpcUHlvZDOfDiuyh5qPMCLrJ1bDw3EuUtx81N/BH3pjQBJQ2HMF5V6iKfeRchVm9kkMtrwxmSdobeA9daBde8GwVlBcFYofS1Jw0vaAy9HeJHQwBUPzIBvGxDc92Rmp/BowJs10wkAONfsBs8HAAAltqngOAO8HZ3o6OiMqcvLy4E1Lwc8H8C5ZndMXdLJa/qNacNLCDBw/O8nFUNWxp/64+tWAwBefe1tHKg7CgC4/9d3ori4EHv3HcDrb26PqVt2602ovvaHaGlpw+8ffSamLqXYmya8jG8mpFy6iGLkWLh4HAwG4+r6j4VBfaPpLgU8IMGO9MLqW2pYQ9aQokuR5dgXIwCC1CUcNMj3hpdvLAdSF54EYpCHooRA0Swomo2pC0kCQpIAkqTA6LmYupgxL0X7m78+aG10NXVkpIwxsAwWXncDCESHLkohfPbpbiT6ZFPPZQ9fC0e58Wi6wTDj6UbT/rQAyiERS2pW4Kc3LQDLRO8miCEAKj7d83FcTxyLJJJJ+9MCqKoq9HomMrgkSThxsgEcZ8AMpwMkSYJlKDA0DVUFiHGWRDJp/4jXwqIo4uFHnkZXdw8AYGbZFXhs3WqQJDkhkkim7E8KoMlkxKbnn8DBunrwUli3e8/+yOAA0HjmHDq7upGXm5PUoDUr7hmWRB5Zt3FYwoime+vtd/H6G9uGJIxouniSyP6H7v8FystnY80jGzIA0MihsMAKu20aTp3JzFb6WCWRuDUvHwByw8cOhw2FBVaYjNzIAba1e3Hfb9aiq7MTNStuBwAsvr4KO3d9GnmKztIS5EyxTJiVSDT7p04tipx/9MnnYc7ORlu7NzMxsK3di5AkDHgGw2DTC+uHBeGJshJJZL/fxyMQEDKbRAiCQDAoQhBDYBkKNE2j4uqrhpUBoiSBIMZfEhkN+1NeiWSqEB2rlUg69md0JRIQRHy86z8jXsqNVRLJlP0jqgNJXXgAgjbCcONmCHUvQ+44NWG2s/rtH5Mt/ciToo0wLH4JBGO6LLazRiJk2vBYy4gHHw/bWSN+LZBKEhkMjzn/CaSiKgQOvJDyFB7L7axUJWNJZDA8IhQA1boPin7KZbMSGfUYyFx9b3hXg/cCsoBA2Z0AoYOaxlcC4+mdyCUDKBzanLFBJ3USyaRMuiSSKZmUSSSTMimTCABUlblRU9kAZ0E39p+eii21c+EL0jHbOwu6sfaWgyjND//U4oP6MmzZnfi79XT7mfQSNi7bh0JzOLG19XBY/89r49pYVebGqhuOosDsh1+gsWV3BXYdd2Q+BlaVuXFv9bHgkSbzk+vfcVRyjHhi47J9cftsXLYf7T36Ix8cLHlo6ydlv6qpPI2qssRZcuOy/Wjp4k5s+2zG+offKqtcUt6kJtNv7S0H0RtkvEufXTB/6bML5je2Wy7UVDbEbF9o9mPDsv2oP5v75vbPS26rP5u3fdXiozDppcwDrKlswOlWy9E//DX09Mt/azh8zzNM1RybF86C7pheVGD240CDeX3NWtfml94Rt+0+Mf3Lm8qbEnpfgdmPs+3G9+564vTT//pM/GrHYduWRP0AYOEMN/5S61xT92Vtfd2XtfWb/vu91fHALyxzw9tnkB/cTD5w+2Ou9375HHtfa7exM5mxRpKFaafdQQKgAcDERs98/foLHrXdaXfoABi8vczhWO2/28/TRR5z2h00gKymNl1ton79oigq6bQ7dE67Q+ew9mb1h4FYYwVESgLAXLSRa+3mWpIdK+UYuPiq89f8+XfT/+ftZQ4vLm9ZmUyfdcsv1M2fWfRaUCK8i8vdK1u6ktuAWPWTsztm24o/cnnYHUsrWzd1+fVJ9XtqxbG3XzFdNcPTawjcueibpxK1t+X26f/9R8a953jub4typOvm2b1XnvUmv8JKWMZcaZffX3XDERRP8cGaFRjWxtPLoZvXY4oxgPBNEsgxBhCUKEzL6Ru+JydS8Ak0giKFgESDJFQoKmCgQzAwIfQEWETzmoBIwd2VNaStu8uEHGO4Buz06zHHFv0dRkefAZ1+PQx0KNK2eIoPLCUj2zDc275qzgcBFWv+cf3IyxgTK2KOzQufEM5kfpGF12eGPSf8DXN+No/87HDWiwYYALw+M6ym8AscAxO++X7xCTRM7EDQzht0Da8v/NWo1dQDAxNCocUXs+303IGHdaptOmYXnh/SLlZbV+fwnwJm6UXEm/ojqgM/PFmJQ81OPHfrtqT7bN23BE8seTflYLvz5DwYGQHLKz5Puo/XZ8aLtT+D1dSDuxbsGQIymmz48DbwIguOESJOcce8XaO3oVpZ8k3Em5KVVAAMFnuOB9as1MbimCBunn04vBmR40ls29Wfgxf1KMn1gBdY+MXUCvK4ANvPndpLzrLzALjBN2VPwrDBksgLYkn1jBMp90nVY2++8vAw3RlPeLNYVZSPAEgjKWP6ZCn4lF+gMdnE08spQb73RQB9aXtgo6tJcNodf8rWz3L//Br340UW3sExEkXrFFKSSUVHqkRfkJZ8QSZk5gS6hw9H+GyDQAclSs41BVmSUIn+toAKIUTJskKoQUknCxKlkISKb/sM0NMyyVAhXW+AlYosfgOgQlUJVadTSUWBKoQoudvPioPbenq5oIUTaRUqenhWKi3oyVIUqKpKREoLggDhF6hQb4CV9LRM9rctMPN6glChp2SdTqeSskwoAECSKnG61fzFR/XsGu+FhmONriYl7TImsjoYKJyZSeB8CoBQo6spqU8TCO1fgE7gDVUNoCYaQA2gBlADqAHURAOoAdQAagA10QCOgfwfNp/hXbfBMCAAAAAASUVORK5CYII=';


class Scratch3LearningMLImagesBlocks {
    /**
     * @return {string} - the name of this extension.
     */
    static get EXTENSION_NAME() {
        return 'learningml-images';
    }

    /**
     * @return {string} - the ID of this extension.
     */
    static get EXTENSION_ID() {
        return 'learningmlImages';
    }

    /**
     * Construct a set of Echidna blocks.
     * @param {Runtime} runtime - the Scratch 3.0 runtime.
     */
    constructor(runtime) {
        /**
         * The Scratch 3.0 runtime.
         * @type {Runtime}
         */
        this.runtime = runtime;

        this.dataset = {
            data: [],
            labels: []
        }

        this.loadMobilenet();
        this.loadTargetModel();

        // Si desde learningML se produce un nuevo modelo hay que cargarlo en
        // Scratch
        let that = this;

        let ch = new BroadcastChannel('new-model');
        ch.addEventListener('message', () => {
            console.log("recargando el modelo en Scratch");
            //if (that.targetModel != null) that.targetModel.dispose();
            that.loadTargetModel();
        });

        this.modelStatus = "";


        // Create a new MicroBit peripheral instance
        //this._peripheral = new Echidna(this.runtime, Scratch3EchidnaBlocks.EXTENSION_ID);
    }

    loadMobilenet() {
        mobilenetModule.load().then(m => {
            console.log(m);
            this.mobilenet = m;
        });
    }

    buildModel() {
        console.log("principio de buildmodel");
        /**
         * Creamos un modelo consistente en una red neuronal con una capa de entrada,
         * una capa oculta y una capa de salida. 
         * 
         * La capa de entrada recibe un tensor de forma (shape) [1024], es decir que hay 
         * 1024 entradas, estas se corresponden con la salida del modelo mobilenet truncada
         * hasta la capa 'conv_preds'. De manera que usamos ese modelo truncado para transformar
         * la imagen en un tensor de shape [1, 1024]. Y es ese tensor el que usamos como entrada
         * de nuestro modelo, es decir, del modelo que se especifica aquí. 
         */
        if (this.targetModel != null) this.targetModel.dispose();

        let that = this;
        this.targetModel = tf.sequential({
            layers: [
                tf.layers.dense({
                    units: 200,
                    inputShape: [1024],
                    activation: 'relu'
                }),
                tf.layers.dense({
                    units: 100,
                    activation: 'relu',
                    kernelInitializer: 'varianceScaling',
                    useBias: true
                }),
                tf.layers.dense({
                    units: that.labels.length,
                    kernelInitializer: 'varianceScaling',
                    useBias: false,
                    activation: 'softmax'
                })
            ]
        });

        // 0.0001 es el learning rate
        const optimizer = tf.train.adam(0.0001);

        this.targetModel.compile({
            optimizer: optimizer,
            loss: 'categoricalCrossentropy',
            metrics: ['accuracy']
        });

        console.log("final de buildmodel");
        console.log(this.targetModel);
    }

    buildDataset(_dataset) {
        if (_dataset == null) return;
        if (!localStorage.getItem('imageLabels')) return;
        this.labels = JSON.parse(localStorage.getItem('imageLabels'));
        this.dataset.labels = [];
        for (let t of this.dataset.data) {
            t.dispose();
        }
        this.dataset.data = []

        this.dataset.labels = _dataset.labels;
        for (let index in _dataset.dataArray) {
            let f32Arr = Float32Array.from(Object.values(_dataset.dataArray[index]));
            this.dataset.data.push(tf.tensor(f32Arr));
        }

        console.log(this.labels);
        console.log(this.dataset);
    }

    loadTargetModel() {
        //tf.disposeVariables();
        if (!localStorage.getItem('imageLabels')) return;
        this.buildDataset(
            JSON.parse(localStorage.getItem('imageDataset'))
        );
        //console.log(this.labels);
        //console.log(this.dataset);
        return tf.loadLayersModel('indexeddb://image-model')
            .then(tm => {
                this.targetModel = tm;
                this.modelStatus = "LOADED";
                return tm;
            });
    }

    encodeDataURI(asset) {
        let data = base64js.fromByteArray(asset.data)
        return `data:${asset.assetType.contentType};base64,${data}`;
    }

    getInfo() {
        return {
            id: Scratch3LearningMLImagesBlocks.EXTENSION_ID,
            name: Scratch3LearningMLImagesBlocks.EXTENSION_NAME,
            //blockIconURI: blockIconURI,
            showStatusButton: false,
            blocks: [{
                    opcode: 'currentCostume',
                    blockType: BlockType.REPORTER,
                    text: formatMessage({
                        id: 'learningml.currentcostume',
                        default: 'Current costume',
                        description: 'Current costume'
                    }),
                },
                {
                    opcode: 'videoImage',
                    blockType: BlockType.REPORTER,
                    text: formatMessage({
                        id: 'learningml.videoimage',
                        default: 'Video image',
                        description: 'Video image'
                    }),
                },
                {
                    opcode: 'videoToggle',
                    text: formatMessage({
                        id: 'learningml.turnvideoonoff',
                        default: 'Turn video [VIDEO_STATE]',
                        description: 'Turn video On/Off'
                    }),
                    arguments: {
                        VIDEO_STATE: {
                            type: ArgumentType.NUMBER,
                            menu: 'VIDEO_STATE',
                            defaultValue: VideoState.ON
                        }
                    }
                },
                {
                    opcode: 'classifyImage',
                    blockType: BlockType.REPORTER,
                    text: formatMessage({
                        id: 'learningml.classifyimage',
                        default: 'Classify image [ITEM]',
                        description: 'Classiffy image'
                    }),

                    arguments: {
                        ITEM: {
                            type: ArgumentType.STRING,
                            defaultValue: 'disfraz'
                        }
                    }
                },
                {
                    opcode: 'confidenceImage',
                    blockType: BlockType.REPORTER,
                    text: formatMessage({
                        id: 'learningml.confidenceimage',
                        default: 'Confidence for image [ITEM]',
                        description: 'Confidence for image'
                    }),

                    arguments: {
                        ITEM: {
                            type: ArgumentType.STRING,
                            defaultValue: 'disfraz'
                        }
                    }
                },
                {
                    opcode: 'addImageToLabel',
                    blockType: BlockType.COMMAND,
                    text: formatMessage({
                        id: 'learningml.addimagetolabel',
                        default: 'Add image [ITEM] to label [LABEL]',
                        description: 'Add image to label'
                    }),
                    arguments: {
                        ITEM: {
                            type: ArgumentType.STRING,
                            defaultValue: "disfraz"
                        },
                        LABEL: {
                            type: ArgumentType.STRING,
                            defaultValue: "label"
                        }
                    }
                },
                {
                    opcode: 'train',
                    blockType: BlockType.COMMAND,
                    text: formatMessage({
                        id: 'learningml.learnfromimage',
                        default: 'Learn from image dataset',
                        description: 'Learn from image dataset'
                    }),
                },
                {
                    opcode: 'whenTrained',
                    blockType: BlockType.HAT,
                    text: 'When model has been trained',
                    text: formatMessage({
                        id: 'learningml.whenmodelhasbeentrained',
                        default: 'When model has been trained',
                        description: 'When model has been trained'
                    }),
                }

            ],
            menus: {

                VIDEO_STATE: {
                    acceptReporters: true,
                    items: this.VIDEO_STATE_INFO
                }
            }
        };
    }

    get VIDEO_STATE_INFO() {
        return [{
                text: 'OFF',
                value: VideoState.OFF
            },
            {
                text: 'ON',
                value: VideoState.ON
            },
            {
                text: 'ON FLIPPED',
                value: VideoState.ON_FLIPPED
            }
        ];
    }

    imageProcess(src) {
        return new Promise((resolve, reject) => {
            let img = new Image();
            img.src = src;
            img.onload = () => resolve(img);
            img.onerror = reject;
        });
    }

    extractFeature(image) {
        image.width = IMAGE_SIZE;
        image.height = IMAGE_SIZE;
        const t_image = tf.browser.fromPixels(image);
        const t_activation = this.mobilenet.infer(t_image, 'conv_preds');
        t_image.dispose();
        return t_activation;
    }

    _classifyImage(image) {

        let t_activation = this.extractFeature(image);
        const prediction = this.targetModel.predict(t_activation);

        const predictions = prediction.dataSync();
        const arr_predictions = Array.from(predictions);
        let results = [];
        for (let i = 0; i < arr_predictions.length; i++) {
            results.push([this.labels[i], arr_predictions[i]]);
        }
        results.sort((a, b) => b[1] - a[1]);
        t_activation.dispose();
        return results;
    }

    c(args, utils) {
        let sprite = utils.target.sprite.costumes[args.ITEM - 1];
        if (sprite == undefined)
            return new Promise((resolve, reject) => { reject("NO IMAGE") });

        let src = this.encodeDataURI(sprite.asset);

        return this.imageProcess(src)
            .then(image => image)
            .then(image => this._classifyImage(image));
    }

    _classifyVideoImage() {

        if (this.runtime.ioDevices.video.provider._video != null) {

            // Get image data from video element
            let video = this.runtime.ioDevices.video.provider._video;
            video.height = IMAGE_SIZE;
            video.width = IMAGE_SIZE;

            return this.imageProcess(this.takeSnapshot(video))
                .then(image => image)
                .then(image => this._classifyImage(image));
        }

        return new Promise((resolve, reject) => { reject("NO VIDEO") });
    }

    classifyImage(args, utils) {
        console.log(this.labels);
        console.log(this.dataset);
        if (args.ITEM == 'video') {
            return this._classifyVideoImage().then(r => r[0][0], e => e);
        } else {
            return this.c(args, utils).then(r => r[0][0], e => e);
        }
    }

    confidenceImage(args, utils) {
        if (args.ITEM == 'video') {
            return this._classifyVideoImage().then(r => r[0][1], e => e);
        } else {
            return this.c(args, utils).then(r => r[0][1], e => e);
        }
    }

    currentCostume(args, utils) {
        //let sprite = utils.target.sprite.costumes[utils.target.currentCostume];
        return utils.target.currentCostume + 1;
    }

    videoImage(args, utils) {
        return InputType.VIDEO;
    }

    videoToggle(args) {
        const state = args.VIDEO_STATE;
        this.globalVideoState = state;
        if (state === VideoState.OFF) {
            this.runtime.ioDevices.video.disableVideo();
        } else {
            this.runtime.ioDevices.video.enableVideo();
            // Mirror if state is ON. Do not mirror if state is ON_FLIPPED.
            this.runtime.ioDevices.video.mirror = state === VideoState.ON;
        }
    }

    takeSnapshot(video) {
        let draw = document.createElement("canvas");
        draw.width = video.videoWidth;
        draw.height = video.videoHeight;
        let context2D = draw.getContext("2d");
        context2D.drawImage(video, 0, 0, video.videoWidth, video.videoHeight);

        let b64Img = draw.toDataURL('image/jpeg', 0.5);

        return b64Img;
    }

    addImageToLabel(args, utils) {
        if (!localStorage.getItem('imageLabels')) return;
        let entry = args.ENTRY;
        let label = args.LABEL;

        if (this.labels.indexOf(label) == -1) {
            this.labels.push(label);
        }

        if (args.ITEM == 'video') {
            if (this.runtime.ioDevices.video.provider._video != null) {

                // Get image data from video element
                let video = this.runtime.ioDevices.video.provider._video;
                video.height = IMAGE_SIZE;
                video.width = IMAGE_SIZE;

                let b64Img = this.takeSnapshot(video);
                console.log(b64Img);

                let img = new Image();
                img.src = b64Img;
                img.onload = () => {
                    const t_activation = this.extractFeature(img);
                    let t = t_activation.squeeze();
                    this.dataset.data.push(t);
                    // Añadimos al array de labels del dataset el índice que el corresponde
                    // a la label en el array labels.
                    this.dataset.labels.push(this.labels.indexOf(label));

                    t_activation.dispose();
                }

                let ch = new BroadcastChannel('model-channel');
                ch.postMessage({ label: label, data: b64Img, image: true });
                this.modelStatus = "OUTDATED";
            }

            return new Promise((resolve, reject) => { reject("NO VIDEO") });
        } else {
            let sprite = utils.target.sprite.costumes[args.ITEM - 1];
            if (sprite == undefined)
                return new Promise((resolve, reject) => { reject("NO IMAGE") });

            let src = this.encodeDataURI(sprite.asset);

            this.imageProcess(src)
                .then(image => {
                    const t_activation = this.extractFeature(image);
                    let t = t_activation.squeeze();
                    this.dataset.data.push(t);
                    // Añadimos al array de labels del dataset el índice que el corresponde
                    // a la label en el array labels.
                    this.dataset.labels.push(this.labels.indexOf(label));

                    t_activation.dispose();
                })
            let ch = new BroadcastChannel('model-channel');
            ch.postMessage({ label: label, data: src, image: true });
            this.modelStatus = "OUTDATED";

            return new Promise((resolve, reject) => { reject("NO IMAGE") });

        }
    }

    train(args, utils) {
        if (!localStorage.getItem('imageLabels')) return;
        console.log(this.labels);
        console.log(this.dataset);
        this.buildModel();
        console.log("después de buildmodel");
        const inputs = tf.stack(this.dataset.data);
        const labels = tf.tidy(() => tf.oneHot(tf.tensor1d(this.dataset.labels, 'int32'), this.labels.length));

        function onBatchEnd(batch, logs) {
            console.log('Accuracy', logs.acc);
        }

        //Train for 5 epochs with batch size of 32.
        this.targetModel.fit(inputs, labels, {
            epochs: 20,
            batchSize: 4,
            callbacks: { onBatchEnd },
            shuffle: true
        }).then(info => {
            console.log('Final accuracy', info.history.acc);
            //this.labeledDataService.state = MLState.TRAINED;
            this.targetModel.save('indexeddb://image-model')
                .then(() => {
                    let ch = new BroadcastChannel('new-model-from-scratch');
                    ch.postMessage('new model saved');
                });
            //this.targetModel.save('downloads://image-model');
            inputs.dispose();
            labels.dispose();
            this.modelStatus = "TRAINED";
            return info;
        });
    }

    whenTrained() {
        if (this.modelStatus == "TRAINED") {
            this.modelStatus = "LOADED";
            return true;
        }
        return false;
    }

}

module.exports = Scratch3LearningMLImagesBlocks;